<?php

namespace App\Controller;

use App\Repository\AddressRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class AddressController extends AbstractController
{
    /**
     * @Route("/address", name="address")
     * @param AddressRepository $addressRepository
     *
     * @return JsonResponse
     */
    public function index(AddressRepository $addressRepository): JsonResponse
    {
        return $this->json(['addresses' => $addressRepository->findAll() ?? 'Нет данных']);
    }
}
