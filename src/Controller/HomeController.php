<?php

declare(strict_types = 1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    /**
     * @Route("/")
     *
     * @return NotFoundHttpException
     */
    public function index (): NotFoundHttpException
    {
        return $this->createNotFoundException('Develop in progress');
    }
}
